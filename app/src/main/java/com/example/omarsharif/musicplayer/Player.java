package com.example.omarsharif.musicplayer;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;

import java.io.File;
import java.util.ArrayList;

public class Player extends AppCompatActivity implements View.OnClickListener {

    static MediaPlayer mp;
    ArrayList<File> mySongs;
    SeekBar sb;
    Button btPlay, btPause, btNext, btPre;
    int position;
    Uri u;
    Thread updateSeekbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        btPlay = findViewById(R.id.btn2);
        btPause = findViewById(R.id.btn3);
        btNext = findViewById(R.id.btn4);
        btPre = findViewById(R.id.btn1);

        btPlay.setOnClickListener(this);
        btPause.setOnClickListener(this);
        btNext.setOnClickListener(this);
        btPre.setOnClickListener(this);

        sb = findViewById(R.id.seekBar);
        updateSeekbar = new Thread() {
            @Override
            public void run() {
                int totalDuration = mp.getDuration();
                int currentPosition = 0;
                while (currentPosition < totalDuration) {
                    try {
                        sleep(500);
                        currentPosition = mp.getCurrentPosition();
                        sb.setProgress(currentPosition);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                //super.run();
            }
        };

        if (mp != null) { //avoid repeatation for opening activity
            mp.stop();
            mp.release();
        }

        Intent i = getIntent();
        Bundle b = i.getExtras();
        mySongs = (ArrayList) b.getParcelableArrayList("songList");
        position = b.getInt("pos", 0);

        u = Uri.parse(mySongs.get(position).toString());
        mp = MediaPlayer.create(getApplicationContext(), u);
        mp.start();
        sb.setMax(mp.getDuration());
        updateSeekbar.start();


        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mp.seekTo(seekBar.getProgress());
            }
        });
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn2:
                mp.start();
                break;
            case R.id.btn3:
                mp.pause();
                break;
            case R.id.btn4:
                mp.stop();
                mp.release();
                position = (position + 1) % mySongs.size();//update pos
                u = Uri.parse(mySongs.get(position).toString());//for last pos
                mp = MediaPlayer.create(getApplicationContext(), u);
                mp.start();
                sb.setMax(mp.getDuration());//update seekbar
                break;
            case R.id.btn1:
                mp.stop();
                mp.release();
                position = (position - 1 < 0) ? mySongs.size() - 1 : position - 1;//update pos
               /* if (position-1<0){
                    position = mySongs.size()-1;
                }else {
                    position=position-1;
                }*/
                u = Uri.parse(mySongs.get(position).toString());//for last pos
                mp = MediaPlayer.create(getApplicationContext(), u);
                mp.start();
                sb.setMax(mp.getDuration());
                break;
        }

    }

}
